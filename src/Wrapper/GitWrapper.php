<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 9/11/16
 * Time: 3:28 PM
 */

namespace RCSI\Wrapper;


use SebastianBergmann\Git\Git;
use Tivie\GitLogParser\Parser;

class GitWrapper extends Git
{
    protected $config;
    protected $repository;
    protected $buildDir;
    protected $repoURL;

    public function __construct($repository, $repositoryURL)
    {
        $this->config = ConfigWrapper::init();
        $this->buildDir = $this->config->get("basePath") . "/build/{$repository}";
        $this->repoURL = $repositoryURL;
        $this->destroy();
        mkdir($this->buildDir, 0777, true);

        parent::__construct($this->buildDir);
    }

    public function getBuildDir()
    {
        return $this->buildDir;
    }

    public function gitTag()
    {
        return $this->execute("tag");
    }

    public function gitClone()
    {
        return $this->execute("clone {$this->repoURL} {$this->buildDir}");
    }

    public function destroy()
    {
        $this->delTree($this->buildDir);
    }

    public function gitLog()
    {
        $parser = new Parser();
        $parser->setGitDir($this->buildDir);
        $logs = $parser->parse();
        return $logs;
    }

    protected function delTree($path)
    {
        if (!file_exists($path)) {
            return true;
        } elseif (!is_dir($path)) {
            return unlink($path);
        }
        $files = array_diff(scandir($path), array('.','..'));
        foreach ($files as $file) {
            (is_dir("{$path}/{$file}") && !is_link($path)) ? $this->delTree("{$path}/{$file}") : unlink("{$path}/{$file}");
        }
        return rmdir($path);
    }

}
