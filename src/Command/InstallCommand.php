<?php
/**
 * Created by PhpStorm.
 * User: msowers
 * Date: 9/29/16
 * Time: 3:04 PM
 */

namespace RCSI\Command;

use RCSI\Config\Config;
use RCSI\Connection\Database\Database;
use RCSI\Exceptions\SetupException;
use RCSI\Wrapper\ConfigWrapper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InstallCommand
 * @package RCSI\Command
 */

class InstallCommand extends Command
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Database
     */
    protected $database;

    public function configure()
    {
        $this
            ->setName('install:packagebuilder')
            ->setDescription('Installs the database and creates the directory tree for first time users')
            ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->config = ConfigWrapper::init();
        $baseDir = $this->config->get('basePath');
        if ($baseDir === null) {
            throw new SetupException("Config file is not set up properly.  Missing basePath");
        }
        $buildDir = "{$baseDir}/build";

        if (!is_dir($buildDir)) {
            $output->writeln("Creating <info>build</info> directory");
            if(mkdir($buildDir, 0775, true) === false) {
                throw new SetupException("Could not create build directory at: {$buildDir}");
            }
        }

        $output->writeln("Connecting to database");

        Database::setConfig($this->config);
        $this->database = Database::init();

        $output->writeln("Cleaning up old tables");
        $tables = array('jobs', 'buildOptions', 'packages', 'owners');
        foreach ($tables as $t) {
            $this->deleteTable($t, $output);
        }

        $output->writeln("Creating data structure");
        $this
            ->buildOwners($output)
            ->buildPackages($output)
            ->buildOptions($output)
            ->buildJobs($output)
            ;


    }

    protected function buildJobs(OutputInterface $output)
    {
        /**
         * @noinspection SqlResolve
         * @noinspection SqlNoDataSourceInspection
         */
        $sql = "CREATE TABLE `jobs` (
            jobID INT(11) NOT NULL AUTO_INCREMENT,
            packageID INT(11),
            packageVersion VARCHAR(255),
            jobProgress FLOAT(3,2) NOT NULL DEFAULT '0',
            jobStatus VARCHAR(255) NULL,
            jobStart TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
            jobEnd TIMESTAMP,
            PRIMARY KEY (jobID),
            FOREIGN KEY (packageID) REFERENCES packages(packageID) ON UPDATE CASCADE
        );";
        $output->write("Creating <info>Jobs</info> table");
        return $this->callDB($sql, $output);
    }


    protected function buildOptions(OutputInterface $output)
    {
        /**
         * @noinspection SqlNoDataSourceInspection
         */
        $sql = "CREATE TABLE `buildOptions` (
            optionID INT(11) NOT NULL AUTO_INCREMENT,
            packageID INT(11) NOT NULL,
            optionType ENUM('require', 'prep', 'build', 'install', 'clean', 'pre', 'post', 'preun', 'postun', 'trigger', 'triggerin', 'triggerun', 'excludeFile', 'setup', 'cleanup'),
            optionValue VARCHAR(255),
            optionOrdinal INT(11),
            PRIMARY KEY (optionID),
            FOREIGN KEY (packageID) REFERENCES packages(packageID) ON DELETE CASCADE
        );";
        $output->write("Creating <info>Options</info> table");
        return $this->callDB($sql, $output);
    }

    protected function buildPackages(OutputInterface $output)
    {
        /**
         * @noinspection SqlNoDataSourceInspection
         */
        $sql = "CREATE TABLE `packages` (
            packageID INT(11) NOT NULL AUTO_INCREMENT,
            packageName VARCHAR(255) NOT NULL,
            packageURL VARCHAR(255) NOT NULL,
            packageKey VARCHAR(255) NOT NULL,
            ownerID INT(11),
            packageLicense VARCHAR(255),
            packageGroup VARCHAR(255),
            packageArch VARCHAR(255),
            packageCompression ENUM('tar', 'tar.gz', 'tar.bz2') NOT NULL DEFAULT 'tar',
            packageSummary TEXT,
            packageDescription TEXT,
            packageAutoDependencyCheck TINYINT(1) NOT NULL DEFAULT '1',
            PRIMARY KEY (packageID),
            FOREIGN KEY (ownerID) REFERENCES owners(ownerID) ON DELETE SET NULL
        );";
        $output->write("Creating <info>Packages</info> table");
        return $this->callDB($sql, $output);
    }

    protected function buildOwners(OutputInterface $output)
    {
        /**
         * @noinspection SqlNoDataSourceInspection
         */
        $sql = "CREATE TABLE `owners` ( ownerID INT(11) NOT NULL AUTO_INCREMENT, ownerName VARCHAR(255) NOT NULL, ownerEmail VARCHAR(255) NOT NULL, PRIMARY KEY (ownerID));";
        $output->write("Creating <info>Owners</info> table");
        return $this->callDB($sql, $output);
    }


    protected function callDB($sql, OutputInterface $output)
    {
        $this->database->query($sql, array());
        $output->write("... <info>Done</info>\n");
        return $this;
    }


    protected function deleteTable($table, OutputInterface $output)
    {
        /**
         * @noinspection SqlNoDataSourceInspection
         */
        $sql = "DROP TABLE IF EXISTS `{$table}`";
        $output->write("Dropping <info>{$table}</info>");
        return $this->callDB($sql, $output);
    }



}
